import { MarkerProps } from "pigeon-marker"
import React, { useMemo } from "react"
import styled, { keyframes } from "styled-components"

const pulseRing = keyframes`
  0% {
    transform: scale(.33);
  }
  80%, 100% {
    opacity: 0;
  }
`
const pulseDot = keyframes`
  0%, 100% {
    transform: scale(.6);
  }
  50% {
    transform: scale(1);
  }
`

const PulsatingDot = styled.div`
  position: absolute;
  transform: translateX(-50%) translateY(-50%);
  width: 20px;
  height: 20px;
  z-index: 5;

  &:before {
    content: "";
    position: relative;
    display: block;
    width: 300%;
    height: 300%;
    box-sizing: border-box;
    margin-left: -100%;
    margin-top: -100%;
    border-radius: 50%;
    background: #ef9a9a;
    animation: ${pulseRing} 2s cubic-bezier(0.215, 0.61, 0.355, 1) infinite;
  }

  &:after {
    content: "";
    position: absolute;
    left: 0;
    top: 0;
    display: block;
    width: 100%;
    height: 100%;
    background: #f44336;
    border-radius: 50%;
    box-shadow: 0 0 8px rgba(0, 0, 0, 0.3);
    animation: ${pulseDot} 2s cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.5s infinite;
  }
`

export function Person(props: MarkerProps) {
  const { top, left } = props

  return <PulsatingDot style={{ top, left }} />
}
