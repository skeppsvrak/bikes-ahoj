import React from "react"
import styled from "styled-components"

import { Gps as GpsIcon } from "./icons/Gps"

export type Props = {
  loading: boolean
  active: boolean
  onClick: () => void
}

const Button = styled.button`
  background: transparent;
  border: none;
  padding: 5px;
  cursor: pointer;
`

type IconStyleProps = { loading: boolean; active: boolean }

const GpsIconThing = styled.span<IconStyleProps>`
  fill: ${props => (props.loading ? "gray" : props.active ? "#f44336" : "black")};
  width: 24px;
  height: 24px;
`

export function GeoButton(props: Props) {
  const { loading, active, onClick } = props

  return (
    <Button title="Zoom to current location" onClick={onClick}>
      <GpsIconThing as={GpsIcon} active={active} loading={loading} />
    </Button>
  )
}
