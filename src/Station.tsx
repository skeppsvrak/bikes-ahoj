import React, { useMemo } from "react"
import styled, { css } from "styled-components"

import { BikeStation } from "./types"

export type StationProps = {
  popupVisible: boolean
  onClick: Function
  station: BikeStation
  expanded: boolean

  // Marker stuff
  top?: number
  left?: number
  anchor?: [number, number]
}

type PopupProps = {
  expanded: boolean
}

const expandedStyle = css`
  flex-direction: column;
  z-index: 6;
  width: 200px;

  &:hover {
    z-index: 7;
  }
`

const darkBlue = "#00449e"
const washedBlue = "#f6fffe"

const Popup = styled.div`
  display: flex;
  position: absolute;
  transform: translate(-50%, -100%);
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.25);
  background-color: white;
  border: 1px solid white;
  z-index: 1;
  cursor: pointer;

  &:hover {
    z-index: 2;
    border-color: black;

    &:after {
      border-top-color: black;
    }
  }

  &:after {
    content: "";
    position: absolute;
    width: 0;
    height: 0;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    left: 50%;
    transform: translate(-50%, 100%);
    z-index: -1;
    bottom: 0;

    border-top: 10px solid white;
  }

  ${({ expanded }: PopupProps) => expanded && expandedStyle};
`

type NumberLabelProps = {
  alternate?: boolean
}

const NumberLabel = styled.div`
  display: inline-block;
  padding: 2px 5px;
  font-size: 1.2em;
  background-color: ${({ alternate }: NumberLabelProps) => (alternate ? washedBlue : darkBlue)};
  color: ${({ alternate }: NumberLabelProps) => (alternate ? "black" : "white")};
  font-weight: bold;
  min-width: 1.5rem;
  text-align: center;
`

const Name = styled.div`
  font-weight: bold;
  padding: 6px;
  text-align: center;
`

function safeRatio(possibleValue: number | undefined, maxValue: number) {
  return possibleValue ? possibleValue / maxValue : 0
}

export function Station(props: StationProps) {
  const { top, left, station, expanded, onClick } = props

  return (
    <Popup
      expanded={expanded}
      style={{ top, left }}
      className="pigeon-click-block"
      title={station.Name}
      onClick={() => onClick()}
    >
      {expanded && <Name>{station.Name}</Name>}
      <NumberLabel>
        {expanded && "Available bikes: "}
        {station.AvailableBikes || "0"}
      </NumberLabel>
      <NumberLabel alternate>
        {expanded && "Available bike stands: "}
        {station.AvailableBikeStands || "0"}
      </NumberLabel>
    </Popup>
  )
}
