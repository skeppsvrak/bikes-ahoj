export function isRetina() {
  const query = "(-webkit-min-device-pixel-ratio: 2), (min-device-pixel-ratio: 2), (min-resolution: 192dpi)"

  return typeof window !== "undefined" && typeof matchMedia !== "undefined" && matchMedia(query).matches
}
