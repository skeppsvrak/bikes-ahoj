import { useEffect, useState } from "react"

export function useShine<T>(data: T, delay: number = 500): boolean {
  const [shine, setShine] = useState(false)

  useEffect(
    () => {
      setShine(true)
      const id = setTimeout(() => {
        setShine(false)
      }, delay)

      return () => {
        clearTimeout(id)
      }
    },
    [data]
  )

  return shine
}
