import fetchJsonp from "fetch-jsonp"
import { useEffect } from "react"

export function usePolling<T>(url: string, setData: (data: T) => void, delay: number = 10000) {
  useEffect(
    () => {
      const id = setInterval(() => {
        fetchJsonp(url)
          .then(r => r.json())
          .then(([data]) => {
            setData(data)
          })
      }, delay)

      return () => {
        clearInterval(id)
      }
    },
    [url]
  )
}
