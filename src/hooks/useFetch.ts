import fetchJsonp from "fetch-jsonp"
import { useCallback, useEffect, useState } from "react"

interface UseFetchOptions {
  pollingInterval?: number
}

export function useFetch<T, U>(
  url: string,
  transform: (o: T) => U,
  options: UseFetchOptions = {}
): [boolean, U | null] {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [data, setData] = useState<U | null>(null)

  const fetchData = async () => {
    setLoading(true)
    const data = await fetch(url).then<T>((r) => r.json())
    setLoading(false)
    setData(transform(data))
  }

  useEffect(() => {
    let intervalId: number | null = null

    if (!data) {
      fetchData().then(() => {
        intervalId = setInterval(fetchData, options.pollingInterval)
      })
    } else {
      intervalId = setInterval(fetchData, options.pollingInterval)
    }

    return () => {
      if (intervalId) {
        clearInterval(intervalId)
      }
    }
  }, [url])

  return [isLoading, data]
}
