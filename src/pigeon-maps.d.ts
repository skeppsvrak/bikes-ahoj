declare module "pigeon-maps" {
  export interface MapProps {
    center: [number, number]
    defaultCenter: [number, number]

    zoom: number
    defaultZoom: number

    width: number
    defaultWidth: number

    height: number
    defaultHeight: number

    provider: Function
    children: React.ReactNode

    animate: boolean
    animateMaxScreens: number

    minZoom: number
    maxZoom: number

    metaWheelZoom: boolean
    metaWheelZoomWarning: string
    twoFingerDrag: boolean
    twoFingerDragWarning: string
    warningZIndex: number

    attribution: any
    attributionPrefix: any

    zoomSnap: boolean
    mouseEvents: boolean
    touchEvents: boolean

    onClick: Function
    onBoundsChanged: (
      data: { center: [number, number]; zoom: number; bounds: { ne: number; sw: number }; initial: boolean }
    ) => void
    onAnimationStart: Function
    onAnimationStop: Function

    // will be set to "edge" from v0.12 onward, defaulted to "center" before
    limitBounds: "center" | "edge"
  }

  export default class Map<T extends Partial<MapProps>> extends React.Component<T> {}
}
