declare module "pigeon-marker" {
  export interface MarkerProps {
    // input, passed to events
    anchor: [number, number]
    payload?: any

    // optional modifiers
    hover?: boolean

    // callbacks
    onClick?: Function
    onContextMenu?: Function
    onMouseOver?: Function
    onMouseOut?: Function

    // pigeon variables
    left?: number
    top?: number

    // pigeon functions
    latLngToPixel?: Function
    pixelToLatLng?: Function
  }

  export default class Marker<T extends MarkerProps> extends React.Component<T> {}
}
