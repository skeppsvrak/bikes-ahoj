import { values } from "lodash-es"
import Cluster from "pigeon-cluster"
import Map, { MapProps } from "pigeon-maps"
import React, { useEffect, useMemo, useState } from "react"
import styled from "styled-components"

import bike from "./assets/marker.png"
import bike2x from "./assets/marker@2x.png"
import { GeoButton } from "./GeoButton"
import { useFetch } from "./hooks/useFetch"
import { Person } from "./Person"
import { Station } from "./Station"
import { BikeStation } from "./types"
import { useGeoPosition } from "./useGeoPosition"
import { isRetina } from "./utils"

type ControlledMapProps = Partial<MapProps> & Required<Pick<MapProps, "center" | "defaultZoom">>

const transform = (stations: BikeStation[]): Record<number, BikeStation> =>
  stations.reduce((acc, station) => ({ ...acc, [station.StationId]: station }), {})

const useFetchOptions = {
  pollingInterval: 10000,
}

const Container = styled.div`
  flex: 1;

  > * {
    position: absolute;
  }
`

const Header = styled.header`
  z-index: 3;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  width: 320px;
  padding: 10px;
  display: flex;
  justify-content: space-between;
  background: white;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.25);
`

const Logo = styled.div`
  font-size: 2rem;
  display: flex;
  align-items: center;
  line-height: 1em;

  b {
    text-transform: uppercase;
    font-size: 0.5em;
    align-self: end;
    color: #00b0ff;
    margin-top: 0.5em;
  }

  img {
    margin: 5px;
    width: 32px;
    height: 32px;
  }
`

function tmsProvider(x: number, y: number, z: number) {
  // return `http://c.tile.stamen.com/watercolor/${z}/${x}/${y}.jpg`
  return `https://a.basemaps.cartocdn.com/light_all/${z}/${x}/${y}.png`
}

export function App() {
  const [loading, stationsById] = useFetch<BikeStation[], Record<number, BikeStation>>(
    `https://data.goteborg.se/SelfServiceBicycleService/v2.0/Stations/${
      process.env.REACT_APP_GBG_KEY
    }?latitude=${57.711547}&longitude=${11.966574}&radius=1000&format=json`,
    transform,
    useFetchOptions
  )

  const stations = useMemo(() => (stationsById ? values(stationsById) : []), [stationsById])
  const [activeStation, setActiveStation] = useState<number | null>(null)
  const [coords, setCoords] = useState<[number, number]>([57.711547, 11.966574])

  const { position, requested: isPositionRequested, request: requestPosition } = useGeoPosition()

  useEffect(() => {
    if (isPositionRequested && position !== null) {
      setCoords([position.coords.latitude, position.coords.longitude])
    }
  }, [position, isPositionRequested])

  if (!stations) {
    return <div>Loading...</div>
  } else {
    return (
      <Container>
        <Map<ControlledMapProps>
          provider={tmsProvider}
          center={coords}
          defaultZoom={15}
          minZoom={13}
          onClick={() => setActiveStation(null)}
          onBoundsChanged={({ center }) => setCoords(center)}
        >
          <Cluster>
            {stations.map((station) => (
              <Station
                expanded={activeStation === station.StationId}
                station={station}
                anchor={[station.Lat, station.Long]}
                key={station.StationId}
                onClick={() => {
                  setActiveStation((prev) => {
                    if (prev === station.StationId) {
                      return null
                    } else {
                      return station.StationId
                    }
                  })
                }}
                popupVisible={activeStation === station.StationId}
              />
            ))}
          </Cluster>
          {isPositionRequested && position && <Person anchor={[position.coords.latitude, position.coords.longitude]} />}
        </Map>
        <Header>
          <Logo>
            {isRetina() ? <img src={bike2x} /> : <img src={bike} />}
            Styr och Ställ<b>Live</b>
          </Logo>
          <GeoButton
            loading={isPositionRequested && !position}
            active={isPositionRequested && !!position}
            onClick={() => requestPosition()}
          />
        </Header>
      </Container>
    )
  }
}
