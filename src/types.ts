export interface BikeStation {
  StationId: number
  Name: string
  Lat: number
  Long: number
  Distance: number
  IsOpen: boolean
  BikeStands: number
  AvailableBikes?: number
  AvailableBikeStands?: number
  LastUpdate: string
}
