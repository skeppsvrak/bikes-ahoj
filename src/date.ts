import { format } from "date-fns"

const dateRegex = /Date\(([0-9]+)\+/

export const formatDate = (dateString: string): string | null => {
  const res = dateString.match(dateRegex)
  if (!res) {
    return res
  }

  const [, unixTime] = res
  return format(new Date(parseInt(unixTime, 10)), "YYYY-MM-DD HH:mm")
}
