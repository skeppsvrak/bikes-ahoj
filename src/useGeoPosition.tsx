// Source: https://github.com/palmerhq/the-platform/blob/f9bc7e278899629773a1f9fda16220c3e5d88cb0/src/useGeoPosition.tsx
// Not imported in package.json because the typescript types are not included there, even
// .. though the hook itself is written in typescript.
import React from "react"

export const useGeoPosition = (positionOptions?: PositionOptions) => {
  const [requested, setRequested] = React.useState(false)
  const [position, setPosition] = React.useState<Position | null>(null)

  React.useEffect(
    () => {
      if (requested) {
        const listener = navigator.geolocation.watchPosition(
          positionUpdate => {
            console.log("New position", positionUpdate)
            setPosition(positionUpdate)
          },
          () => null,
          positionOptions
        )

        return () => navigator.geolocation.clearWatch(listener)
      }
    },
    [requested]
  )

  const request = () => setRequested(!requested)

  return { position, requested, request }
}
